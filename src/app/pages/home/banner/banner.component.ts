import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

  slides = [
    {img: "assets/images/banners/3.jpg"},
    {img: "assets/images/banners/2.jpg"},
    {img: "assets/images/banners/1.jpg"}
  ];
  slideConfig = {
    "slidesToShow": 1, 
    "slidesToScroll": 1, 
    autoplay: true, 
    dots: true,
    infinite: true,
    pauseOnHover: false,
    prevArrow: true,
    nextArrow: true
  };

  
  slickInit(e) {
    console.log('slick initialized');
  }
  
  breakpoint(e) {
    console.log('breakpoint');
  }
  
  afterChange(e) {
    console.log('afterChange');
  }
  
  beforeChange(e) {
    console.log('beforeChange');
  }

  constructor() { }

  ngOnInit() {
  }

}
