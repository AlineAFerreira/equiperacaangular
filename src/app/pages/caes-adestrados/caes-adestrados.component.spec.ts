import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaesAdestradosComponent } from './caes-adestrados.component';

describe('CaesAdestradosComponent', () => {
  let component: CaesAdestradosComponent;
  let fixture: ComponentFixture<CaesAdestradosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaesAdestradosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaesAdestradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
