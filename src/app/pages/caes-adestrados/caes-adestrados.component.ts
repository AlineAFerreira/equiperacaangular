import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http/http.service';

@Component({
  selector: 'app-caes-adestrados',
  templateUrl: './caes-adestrados.component.html',
  styleUrls: ['./caes-adestrados.component.scss']
})
export class CaesAdestradosComponent implements OnInit {
  dogs = [];

  constructor(
    private httpService: HttpService
  ) { }

  ngOnInit() {
    this.httpService.getDogs().subscribe(response => {
      this.dogs = response.dogs;
    })
  }

}
