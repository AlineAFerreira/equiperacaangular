import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../../services/http/http.service';

@Component({
  selector: 'app-cao-adestrado',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './cao-adestrado.component.html',
  styleUrls: ['./cao-adestrado.component.scss']
})
export class CaoAdestradoComponent implements OnInit {
  dog: any = {};

  constructor(
    private route: ActivatedRoute,
    private httpService: HttpService
  ) { }

  ngOnInit() {
    this.getDog();
  }
  
  getDog() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.httpService.getDog(id).subscribe(response => {
      this.dog = response.dogs.find(dog => {return dog.id === id;});
      // this.dog = response.dogs.filter(dog => {return dog.id === id;})[0];
      console.log(this.dog)
    });
  }
}
