import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaoAdestradoComponent } from './cao-adestrado.component';

describe('CaoAdestradoComponent', () => {
  let component: CaoAdestradoComponent;
  let fixture: ComponentFixture<CaoAdestradoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaoAdestradoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaoAdestradoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
