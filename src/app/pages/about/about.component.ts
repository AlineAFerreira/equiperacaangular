import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpService } from '../../services/http/http.service';

@Component({
  selector: 'app-about',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  images = [];
  videos = [];

  constructor(
    private httpService: HttpService
  ) { }

  ngOnInit() {
    this.httpService.getPhotos().subscribe(response => {
      this.images = response.about.images;
    })
    this.httpService.getVideos().subscribe(response => {
      this.videos = response.about.videos;
    })
  }

}
