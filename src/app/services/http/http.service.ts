import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient
  ) { }

  getDogs(): Observable<any> {
    return this.http.get('../../assets/dogs.json');
  }

  getDog(id: number): Observable<any> {
    return this.http.get('../../assets/dogs.json');
  }

  getPhotos(): Observable<any> {
    return this.http.get('../../assets/dogs.json');
  }

  getVideos(): Observable<any> {
    return this.http.get('../../assets/dogs.json');
  }
}
