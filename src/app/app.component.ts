import { Component } from '@angular/core';
import 'magnific-popup';

declare var jquery:any;
declare var $ :any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    ngOnInit() {
        $('body').addClass('');
    }

    ngAfterViewChecked() {
        $('.gallery').magnificPopup({
          delegate: 'a',
          type: 'image',
          fixedContentPos: !1,
          removalDelay: 100,
          closeBtnInside: !0,
          preloader: !1,
          gallery:{
              enabled: true
          }
        });

        $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
          });

          
      }
}
