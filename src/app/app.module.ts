import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { AboutComponent } from './pages/about/about.component';
import { HomeComponent } from './pages/home/home.component';
import { ContactComponent } from './pages/home/contact/contact.component';
import { BannerComponent } from './pages/home/banner/banner.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ServicesComponent } from './pages/home/services/services.component';
import { AgendeComponent } from './pages/home/agende/agende.component';
import { HowItWorksComponent } from './pages/home/how-it-works/how-it-works.component';
import { CaesAdestradosComponent } from './pages/caes-adestrados/caes-adestrados.component';

// Import your library
import * as $ from 'jquery';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { IsotopeModule } from 'ngx-isotope';

import { HttpClientModule } from '@angular/common/http';
import { HttpService } from './services/http/http.service';
import { CaoAdestradoComponent } from './pages/cao-adestrado/cao-adestrado.component';
import { ContactAsideComponent } from './shared/contact-aside/contact-aside.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AboutComponent,
    HomeComponent,
    ContactComponent,
    BannerComponent,
    FooterComponent,
    ServicesComponent,
    AgendeComponent,
    HowItWorksComponent,
    CaesAdestradosComponent,
    CaoAdestradoComponent,
    ContactAsideComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SlickCarouselModule,
    IsotopeModule,
    HttpClientModule
  ],
  providers: [
    HttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
