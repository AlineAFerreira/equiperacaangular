import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { HomeComponent } from './pages/home/home.component';
import { CaesAdestradosComponent } from './pages/caes-adestrados/caes-adestrados.component';
import { CaoAdestradoComponent } from './pages/cao-adestrado/cao-adestrado.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'sobre', component: AboutComponent },
  { path: 'caes-adestrados', component: CaesAdestradosComponent },
  { path: 'cao-adestrado/:id', component: CaoAdestradoComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
