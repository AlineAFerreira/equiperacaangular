import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Input() title: string;

    constructor() { }

    ngOnInit() {
    }
    public runTo(target, space, delay) {
        $('html, body').stop().animate({
            scrollTop: $(target).offset().top - space
        }, delay)
    }
}
