import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactAsideComponent } from './contact-aside.component';

describe('ContactAsideComponent', () => {
  let component: ContactAsideComponent;
  let fixture: ComponentFixture<ContactAsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactAsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactAsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
